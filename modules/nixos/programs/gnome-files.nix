{
  config,
  lib,
  pkgs,
  ...
}:

let
  cfg = config.programs.gnome.gnome-files;
in
{
  options = {
    programs.gnome.gnome-files.enable = lib.mkOption {
      description = ''
        Whether to install GNOME Files (Nautilus) and enable additional
        configuration, e.g. for thumbnails, file previews, network filesystems
      '';
      type = lib.types.bool;
      default = false;
    };
  };

  config = lib.mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      gnome.nautilus

      # Generate thumbnails for video files
      ffmpegthumbnailer
    ];

    services = {
      # Enable file previewing (press Space when a file is selected)
      gnome.sushi.enable = lib.mkDefault true;

      # Enable virtual filesystem support (trash://, smb://, etc.)
      gvfs.enable = lib.mkDefault true;
    };
  };
}
