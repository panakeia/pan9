{
  pkgs,
  ...
}:

{
  environment.systemPackages = with pkgs; [ samba ];

  services.samba = {
    enable = true;
    settings = {
      global = {
        "invalid users" = [ "root" ];
        "bind interfaces only" = "no";
      };
      private = {
        browseable = "no";
        "read only" = false;
        "guest ok" = "no";
        path = "/srv/tank";
      };
    };
  };
}
