{
  config,
  lib,
  pkgs,
  ...
}:

let
  domain = "pan.ax";
in
{
  sops.secrets = {
    digitalocean_credentials = { };
  };

  security.acme = {
    acceptTerms = true;
    defaults.dnsResolver = "9.9.9.9:53";
    certs = {
      "${domain}" = {
        email = "${config.custom.users.pan.email}";
        group = "nginx";
        # using DNS authentication for server not exposed to the Internet
        webroot = lib.mkForce null;
        dnsProvider = "digitalocean";
        # create a token at https://cloud.digitalocean.com/account/api/tokens/new
        # sh -c 'echo "DO_AUTH_TOKEN=${TOKEN}" > "digitalocean_credentials"'
        credentialsFile = "/run/secrets/digitalocean_credentials";
        extraDomainNames = [ "*.${domain}" ];
      };
    };
  };

  services.nginx =
    let
      subdomains = {
        "git" = 3000;
        "music" = 4533;
        "calendar" = 5232;
        "radio" = 6601;
        "torrents" = 9091;
        "ci" = 8000;
        "wiki" = 8080;
        "chat" = 8081;
        "passwords" = 8083;
      };
    in
    {
      enable = true;
      recommendedProxySettings = true;
      virtualHosts =
        let
          mkVirtualHost =
            subdomain: port:
            lib.nameValuePair ("${subdomain}.${domain}") ({
              useACMEHost = domain;
              onlySSL = true;
              locations."/" = {
                proxyPass = "http://127.0.0.1:${builtins.toString port}";
                extraConfig = ''
                  proxy_pass_request_headers on;
                '';
              };
            });
        in
        lib.recursiveUpdate {
          "${domain}" = {
            useACMEHost = domain;
            onlySSL = true;
            locations."/".extraConfig = ''
              alias ${pkgs."${domain}"}/;
            '';
          };
        } (lib.mapAttrs' mkVirtualHost subdomains);
    };
}
