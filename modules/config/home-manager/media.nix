{
  config,
  lib,
  pkgs,
  ...
}:

lib.mkMerge [
  {
    home.sessionVariables = {
      # Fullscreen SDL windows on the primary display instead of all displays
      SDL_VIDEO_FULLSCREEN_DISPLAY = 0;
      SDL_VIDEO_FULLSCREEN_HEAD = 0;
    };

    programs.mpv = {
      enable = lib.mkDefault true;
      config = {
        profile = "gpu-hq";
        hwdec = "auto-safe";
        scale = "ewa_lanczossharp";
        cscale = "ewa_lanczossharp";
        video-sync = "display-resample";
        interpolation = "yes";
        tscale = "oversample";
        ytdl-format = "bestvideo+bestaudio";
        keepaspect = "yes";
        keep-open = "yes";
        screenshot-directory = "${lib.custom.pathToUserDir "pictures"}/screenshots";
        volume = 50;
      };
      bindings = {
        MBTN_FORWARD = "ignore";
        MBTN_RIGHT = "ignore";
        q = "ignore";
      };
      profiles = {
        headphones = {
          ad-lavc-downmix = "no";
          audio-channels = "stereo";
        };
      };
    };

    programs.ncmpcpp = {
      enable = true;
      settings = {
        external_editor = config.home.sessionVariables.EDITOR;
        use_console_editor = true;
        startup_screen = "media_library";
        header_visibility = false;
        statusbar_visibility = false;
        titles_visibility = false;
        display_bitrate = false;
        enable_window_title = true;
        song_window_title_format = "ncmpcpp - %t";
        playlist_display_mode = "classic";
        song_list_format = "$4{%A}|{%a}$4 $8-$1 $2%t$1 $R$7%l";
        song_status_format = "{%b}";
        now_playing_prefix = "―― ";
        now_playing_suffix = "$/b";
        playlist_disable_highlight_delay = 1;
        progressbar_look = "---";
        cyclic_scrolling = true;
        media_library_primary_tag = "album_artist";
        media_library_albums_split_by_date = false;
        discard_colors_if_item_is_selected = true;
      };
    };

    xdg.configFile =
      let
        ytdlConfig = ''
          -o ${lib.custom.pathToUserDir "download"}/%(title)s.%(ext)s
        '';
      in
      {
        "yt-dlp/config".text = ytdlConfig;
        "youtube-dl/config".text = ytdlConfig;
        "streamlink/config".text = ''
          player=mpv
          default-stream=best
        '';
      };
  }

  (lib.custom.mkLinux {
    programs.beets = {
      enable = lib.mkDefault true;
      package = pkgs.beets-unstable;
      settings = {
        plugins = [
          "discogs"
          "lyrics"
        ];
      };
    };

    programs.obs-studio = {
      enable = lib.mkDefault true;
      package = pkgs.obs-studio;
    };

    services.fluidsynth = {
      enable = lib.mkDefault true;
      soundService = "pipewire-pulse";
    };
  })
]
