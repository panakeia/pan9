{ config, pkgs, ... }:

{
  dconf.settings = {
    "org/gnome/mutter" = {
      experimental-features = [ "kms-modifiers" ];
    };
  };

  home.packages = with pkgs; [ wl-clipboard ];

  home.sessionVariables.MOZ_ENABLE_WAYLAND = 1;

  programs.mpv.config.gpu-context = "wayland";

  wayland.windowManager.sway = {
    enable = true;
    config = {
      fonts.size = 13.0;
      bars = [ ];
      modifier = "Mod4";
      focus.followMouse = false;
      input = {
        "type:keyboard" = {
          xkb_options = "ctrl:nocaps";
        };
      };
      keybindings =
        let
          cfg = config.wayland.windowManager.sway.config;
          modifier = cfg.modifier;
        in
        {
          "${modifier}+${cfg.left}" = "focus left";
          "${modifier}+${cfg.right}" = "focus right";
          "${modifier}+${cfg.up}" = "focus up";
          "${modifier}+${cfg.down}" = "focus down";

          "${modifier}+Shift+${cfg.left}" = "move left";
          "${modifier}+Shift+${cfg.right}" = "move right";
          "${modifier}+Shift+${cfg.up}" = "move up";
          "${modifier}+Shift+${cfg.down}" = "move down";

          "${modifier}+f" = "floating toggle";
          "${modifier}+m" = "fullscreen toggle";
          "${modifier}+Control+y" = "sticky toggle";
          "${modifier}+w" = "kill";

          "${modifier}+1" = "workspace number 1";
          "${modifier}+2" = "workspace number 2";
          "${modifier}+3" = "workspace number 3";
          "${modifier}+4" = "workspace number 4";
          "${modifier}+5" = "workspace number 5";
          "${modifier}+6" = "workspace number 6";
          "${modifier}+7" = "workspace number 7";
          "${modifier}+8" = "workspace number 8";
          "${modifier}+9" = "workspace number 9";
          "${modifier}+BracketLeft" = "workspace prev_on_output";
          "${modifier}+BracketRight" = "workspace next_on_output";

          "${modifier}+Shift+1" = "move container to workspace number 1";
          "${modifier}+Shift+2" = "move container to workspace number 2";
          "${modifier}+Shift+3" = "move container to workspace number 3";
          "${modifier}+Shift+4" = "move container to workspace number 4";
          "${modifier}+Shift+5" = "move container to workspace number 5";
          "${modifier}+Shift+6" = "move container to workspace number 6";
          "${modifier}+Shift+7" = "move container to workspace number 7";
          "${modifier}+Shift+8" = "move container to workspace number 8";
          "${modifier}+Shift+9" = "move container to workspace number 9";
          "${modifier}+Shift+BracketLeft" = "move container to workspace prev_on_output";
          "${modifier}+Shift+BracketRight" = "move container to workspace next_on_output";

          "${modifier}+Space" = "exec ${pkgs.wofi}/bin/wofi --show drun";
          "${modifier}+Return" = "exec ${pkgs.foot}/bin/foot";
          "${modifier}+Control+3" = "exec ${pkgs.grim}/bin/grim | ${pkgs.wl-clipboard}/bin/wl-copy -t image/png";
          "${modifier}+Control+4" = ''exec ${pkgs.grim}/bin/grim -g "$(${pkgs.slurp}/bin/slurp)" | ${pkgs.wl-clipboard}/bin/wl-copy -t image/png'';

          "${modifier}+Shift+r" = "reload";
          "${modifier}+Shift+q" = "exit";
        };
    };
  };
}
