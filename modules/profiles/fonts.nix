{
  config,
  lib,
  pkgs,
  ...
}@args:
lib.custom.mkProfile args {
  name = "fonts";
  description = "Whether to enable custom font configuration";
  default = config.custom.profiles.desktop.enable;
  settings = lib.mkMerge [
    {
      fonts = {
        fontDir.enable = lib.mkDefault true;
        packages =
          with pkgs;
          [
            # Serif
            liberation_ttf
            libertinus
            poly

            # Sans
            gyre-fonts
            inter
            public-sans

            # Bitmap
            tewi-font
            cozette

            # Monospace
            fira-code
            ibm-plex
            iosevka
          ]
          ++ lib.optionals pkgs.stdenv.hostPlatform.isLinux [
            # Bitmap
            cherry
            siji

            # Monospace
            iosevka-bin

            # Emoji
            twemoji-color-font
          ];
      };
    }

    (lib.custom.mkLinux {
      # Disable hinting and subpixel antialiasing.
      fonts.fontconfig.hinting.enable = false;
      fonts.fontconfig.subpixel.rgba = "none";

      # Configure default fonts. If a glyph is not supported
      # by a particular font, the next font in the list
      # will be tried.
      fonts.fontconfig.defaultFonts = {
        monospace = [
          "Iosevka"
        ] ++ lib.optionals config.custom.profiles.japanese.enable [ "Migu 1M" ] ++ [ "DejaVu Sans Mono" ];
        sansSerif = [
          "Inter"
        ] ++ lib.optionals config.custom.profiles.japanese.enable [ "IPAPGothic" ] ++ [ "DejaVu Sans" ];
        serif = [ "DejaVu Serif" ] ++ lib.optionals config.custom.profiles.japanese.enable [ "IPAPMincho" ];
        emoji = [
          "Twitter Color Emoji"
          "Noto Color Emoji"
        ];
      };
    })
  ];
}
