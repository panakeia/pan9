{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  inherit (lib.custom) mkLinux mkDarwin mkMergeIf;
  cfg = config.custom.profiles.desktop;
in
{
  options = {
    custom.profiles.desktop = {
      enable = mkEnableOption "Default settings for desktop PCs.";
    };
  };

  config = mkMergeIf cfg.enable [
    {
      # Enable man pages, etc.
      documentation.enable = true;

      # Source ~/.profile when loading shell, if it exists.
      #
      # This will otherwise only happen if the shell is an interactive login shell,
      # which is not the case most of the time when logging into a graphical environment:
      # https://github.com/NixOS/nixpkgs/issues/5200
      # https://github.com/nix-community/home-manager/issues/1011
      #
      # This may not do anything on wayland, probably depending on the compositor used:
      # https://unix.stackexchange.com/questions/317282/set-environment-variables-for-gnome-on-wayland-and-bash-on-virtual-terminals-or
      environment.loginShellInit = ''
        if [ -e $HOME/.profile ]
        then
          . $HOME/.profile
        fi
      '';
    }

    (mkLinux {
      # Deprioritize Nix daemon process.
      nix = {
        daemonIOSchedClass = mkDefault "idle";
        daemonCPUSchedPolicy = mkDefault "idle";
      };

      # Allow nonfree firmware.
      hardware.enableRedistributableFirmware = mkDefault true;

      # Enable graphics drivers.
      hardware.graphics.enable = true;

      # Enable splash screen on boot.
      boot.plymouth.enable = mkDefault true;

      # Enable i2c device driver (needed to control external displays via ddcutil)
      boot.kernelModules = [ "i2c-dev" ];

      # Enable systemd in stage-1 initrd.
      boot.initrd.systemd.enable = mkDefault true;

      # Enable dictd dictionary server.
      services.dictd = {
        enable = mkDefault true;
        DBs = with pkgs.dictdDBs; [ wiktionary ];
      };

      # Enable commonly needed GNOME services for:
      # - Local contacts/calendars storage
      # - Secrets management
      services.gnome.evolution-data-server.enable = mkDefault true;
      services.gnome.gnome-keyring.enable = mkDefault true;

      services.udisks2.enable = mkDefault true;

      services.printing = {
        enable = mkDefault true;
        webInterface = mkDefault false;
        stateless = mkDefault true;
        drivers = [ pkgs.brlaser ];
      };

      services.usbmuxd.enable = mkDefault true;

      services.flatpak.enable = mkDefault true;

      # Add X session, managed by home-manager
      # Requires `xsession.scriptPath = ".xsession-hm"` in home-manager configuration
      # See https://github.com/rycee/home-manager/issues/391
      services.xserver.desktopManager.session = [
        {
          name = "home-manager";
          start = ''
            ${pkgs.stdenv.shell} $HOME/.xsession-hm &
            waitPID=$!
          '';
        }
      ];

      programs.dconf.enable = true;

      qt.enable = true;

      xdg.portal.enable = true;
      xdg.portal.wlr.enable = true;
      xdg.portal.config.common.default = "*";

      home-manager.sharedModules = [
        {
          dconf.enable = mkDefault true;
          xdg.userDirs.enable = mkDefault true;
        }
      ];
    })

    (mkDarwin {
      system = {
        defaults = {
          dock = {
            # Speed up Mission Control animations.
            expose-animation-duration = 0.15;

            # Don't show recent applications.
            show-recents = false;

            # Automatically hide the Dock.
            autohide = true;

            # Disable animation when launching applications from the Dock.
            launchanim = false;

            # Change the Dock minimize effect.
            mineffect = "scale";
          };

          finder = {
            # Show file extensions in Finder.
            AppleShowAllExtensions = true;
          };
        };
      };

      programs.gnupg.agent = {
        enable = lib.mkDefault true;
        enableSSHSupport = lib.mkDefault true;
      };
    })
  ];
}
