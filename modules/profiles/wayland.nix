{ config, lib, ... }@args:
lib.custom.mkProfile args {
  name = "wayland";
  description = "Whether to use Wayland display server";
  default = config.custom.profiles.desktop.enable;
  settings = lib.custom.mkLinux (
    lib.mkMerge [
      {
        # Enable Wayland Ozone backend.
        environment.sessionVariables.NIXOS_OZONE_WL = "1";

        services.xserver.displayManager.gdm.wayland = true;
        programs.xwayland.enable = true;
      }
    ]
  );
}
