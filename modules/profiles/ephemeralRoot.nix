{
  config,
  lib,
  pkgs,
  ...
}:

with lib;

let
  inherit (lib.custom) mkLinux mkMergeIf;

  cfg = config.custom.profiles.ephemeralRoot;
in
{
  options = {
    custom.profiles.ephemeralRoot = {
      enable = mkEnableOption ''
        Whether to roll back / to a blank ZFS snapshot at boot,
        as described in https://grahamc.com/blog/erase-your-darlings
      '';
      zpool = mkOption {
        default = "rpool";
        type = types.str;
        description = "Name of the zpool to roll back.";
      };
      persistDir = mkOption {
        default = "/persist";
        type = types.str;
        description = "Where to persist state by default.";
      };
    };
  };

  config = mkMergeIf cfg.enable [
    (mkLinux {
      boot.initrd.systemd.services.zfs-rollback = {
        description = "Rollback ZFS datasets to blank snapshot";
        wantedBy = [ "initrd.target" ];
        after = [ "zfs-import-${cfg.zpool}.service" ];
        before = [ "sysroot.mount" ];
        path = [ pkgs.zfs ];
        unitConfig.DefaultDependencies = "no";
        serviceConfig.Type = "oneshot";
        script = ''
          zfs rollback -r ${cfg.zpool}/local/root@blank
        '';
      };

      environment.persistence.${cfg.persistDir} = {
        directories =
          [
            "/etc/nixos"
            "/var/lib/nixos"
            "/var/log"
          ]
          ++ lib.optionals config.networking.wireguard.enable [ "/etc/wireguard" ]
          ++ lib.optionals config.networking.networkmanager.enable [
            "/etc/NetworkManager/system-connections"
          ]
          ++ lib.optionals config.hardware.bluetooth.enable [ "/var/lib/bluetooth" ]
          ++ lib.optionals config.services.usbmuxd.enable [ "/var/lib/lockdown" ]
          ++ lib.optionals config.services.samba.enable [ "/var/lib/samba" ]
          ++ lib.optionals config.services.gitea.enable [ "/var/lib/gitea" ]
          ++ lib.optionals config.services.forgejo.enable [ "/var/lib/forgejo" ]
          ++ lib.optionals config.services.radicale.enable [ "/var/lib/radicale" ]
          ++ lib.optionals config.services.flatpak.enable [ "/var/lib/flatpak" ]
          ++ lib.optionals config.security.acme.acceptTerms [ "/var/lib/acme" ]
          ++ lib.optionals config.services.sourcehut.enable [ "/var/lib/git" ]
          ++ lib.optionals config.services.postgresql.enable [ "/var/lib/postgresql" ]
          ++ lib.optionals config.services.owncast.enable [ "/var/lib/owncast" ]
          ++ lib.optionals config.services.woodpecker-server.enable [ "/var/lib/private/woodpecker-server" ]
          ++ lib.optionals config.services.tiddlywiki.enable [ "/var/lib/tiddlywiki" ]
          ++ lib.optionals config.services.navidrome.enable [
            "/var/lib/navidrome"
            "/var/lib/private/navidrome"
          ];
        files = [ "/etc/machine-id" ] ++ lib.optionals config.users.mutableUsers [ "/etc/shadow" ];
      };

      services.openssh = {
        hostKeys = [
          {
            path = "${cfg.persistDir}/etc/ssh/ssh_host_ed25519_key";
            type = "ed25519";
          }
          {
            path = "${cfg.persistDir}/etc/ssh/ssh_host_rsa_key";
            type = "rsa";
            bits = 4096;
          }
        ];
      };

      home-manager.backupFileExtension = ".home-manager-backup";
    })
  ];
}
