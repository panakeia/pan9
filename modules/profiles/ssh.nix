{ config, lib, ... }@args:
lib.custom.mkProfile args {
  name = "ssh";
  description = ''
    Whether to enable SSH and fail2ban. Additionally, whether to allow sudo
    authentication via the SSH agent for non-desktop systems.
  '';
  default = false;
  settings = lib.custom.mkLinux {
    security.pam = {
      sshAgentAuth.enable = lib.mkDefault (!config.custom.profiles.desktop.enable);
      services.sudo.sshAgentAuth = lib.mkDefault (!config.custom.profiles.desktop.enable);
    };

    services = {
      fail2ban.enable = lib.mkDefault true;
      openssh.enable = lib.mkDefault true;
    };
  };
}
