{ config, lib, ... }@args:
lib.custom.mkProfile args {
  name = "networking";
  description = "Whether to support networking";
  default = true;
  settings = lib.mkMerge [
    (lib.custom.mkLinux (
      lib.mkMerge [
        {
          # Enable a one-shot service for updating the system timezone via GeoIP.
          services.tzupdate.enable = lib.mkDefault true;

          networking.useNetworkd = lib.mkDefault true;
        }

        (lib.mkIf config.custom.profiles.desktop.enable {
          networking = {
            wireless.iwd.enable = lib.mkDefault true;
            networkmanager = {
              enable = lib.mkDefault true;
              # Randomize MAC address when connecting to Wi-Fi networks.
              wifi.macAddress = "random";
              wifi.backend = "iwd";
            };
            nameservers = lib.custom.dnsServers.withTls;
          };

          services = {
            geoclue2.enable = lib.mkDefault true;
            gnome.gnome-online-accounts.enable = lib.mkDefault true;
            resolved = {
              enable = lib.mkDefault true;
              fallbackDns = lib.custom.dnsServers.raw;
              # TODO: change "opportunistic" to "yes" once this works reliably
              extraConfig = ''
                DNSOverTLS="opportunistic"
                DNS=${lib.concatStringsSep " " lib.custom.dnsServers.withTlsResolved}
              '';
            };
          };
        })
      ]
    ))

    (lib.custom.mkDarwin { networking.knownNetworkServices = [ "Wi-Fi" ]; })
  ];
}
