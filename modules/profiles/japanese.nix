{
  config,
  lib,
  pkgs,
  ...
}@args:
lib.custom.mkProfile args {
  name = "japanese";
  description = "Whether to enable Japanese IME, locale, fonts, etc.";
  default = config.custom.profiles.desktop.enable;
  settings = lib.mkMerge [
    {
      fonts.packages = lib.attrValues {
        inherit (pkgs)
          ipafont
          migu
          rounded-mgenplus
          sarasa-gothic
          ;
      };
    }

    (lib.custom.mkLinux {
      # Enable Japanese locale.
      i18n.supportedLocales = [ "ja_JP.UTF-8/UTF-8" ];

      # Enable fcitx input method for inserting Japanese characters
      i18n.inputMethod = {
        enabled = "fcitx5";
        fcitx5.addons = with pkgs; [ fcitx5-mozc ];
      };
    })
  ];
}
