{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}@args:
lib.custom.mkProfile args {
  name = "hardened";
  description = "Whether to enable NixOS and Linux kernel hardening";
  default = true;
  settings = lib.custom.mkLinux (
    lib.mkMerge [
      (import "${modulesPath}/profiles/hardened.nix" (args // { inherit pkgs; }))

      {
        # Reverse defaults from the hardened profile.
        security.lockKernelModules = false;
        security.allowUserNamespaces = true;

        # Use a hardened ZFS-compatible kernel.
        boot.kernelPackages = lib.mkForce pkgs.linuxKernel.packages.linux_6_6_hardened;

        # Use "libc" malloc instead of "scudo", as "scudo" causes segfaults in
        # openssh and firefox: https://github.com/NixOS/nixpkgs/pull/73763
        environment.memoryAllocator.provider = "libc";
      }

      (lib.mkIf config.custom.profiles.desktop.enable {
        # Enable SMT for better performance.
        security.allowSimultaneousMultithreading = true;
      })

      (lib.mkIf config.programs.steam.enable {
        # Proton requires the ability to create user namespaces, so allow this when Steam
        # is enabled.
        #
        # TODO: According to https://github.com/ValveSoftware/steam-runtime/issues/297,
        # another sufficient fix could be:
        #
        # `environment.systemPackages = with pkgs; [ bubblewrap ];`
        #
        # which would be preferable assuming it actually works, and even a decent candidate
        # for upstreaming.
        boot.kernel.sysctl = {
          "kernel.unprivileged_userns_clone" = true;
        };
      })
    ]
  );
}
