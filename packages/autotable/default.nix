{
  stdenv,
  lib,
  mkYarnPackage,
  fetchgit,
  blender,
  inkscape,
  gnumake,
  nodejs,
  python3,
}:

stdenv.mkDerivation rec {
  pname = "autotable";
  version = "2021-01-23";

  src = fetchgit {
    url = "https://github.com/pwmarcz/autotable.git";
    rev = "8a30c32b7ac0f4c2649d43a1364200c6ae197d51";
    hash = "sha256:1VS8KDydMohpgGdBwVekv+fFpMzvlCb017UcjO25yFY=";
    fetchLFS = true;
  };

  autotable-js-modules = mkYarnPackage {
    pname = "${pname}-js-modules";
    inherit src version;
  };

  nativeBuildInputs = [
    blender
    inkscape
    gnumake
    nodejs
    python3
  ];

  buildPhase = ''
    ln -s ${autotable-js-modules}/libexec/*/node_modules node_modules
    make build
  '';

  installPhase = ''
    mkdir -p $out
    cp -R build/* $out
  '';

  # TODO: This might build on x86_64-darwin as well, but mkYarnPackage
  # eagerly tries to build the yarn.nix derivation when the package is
  # evaluated, leading to the following error when running `nix flake check`:
  #
  # "a 'x86_64-darwin' with features {} is required to build yarn.nix.drv,
  # but I am a $PLATFORM with features {}"
  meta = {
    description = "An online mahjong table";
    homepage = "https://github.com/pwmarcz/autotable";
    license = lib.licenses.mit;
    platforms = [ "x86_64-linux" ];
    broken = true;
  };
}
