{
  stdenvNoCC,
  lib,
  fetchFromGitHub,
  gnumake,
}:

stdenvNoCC.mkDerivation rec {
  pname = "fleck";
  version = "0.1.2";

  src = fetchFromGitHub {
    owner = "chr15m";
    repo = "flk";
    rev = "v${version}";
    hash = "sha256:15jf2j3c62fv8mf07w0rhhg0cyhflaim0iwzcy3rcray649wz6v2";
    fetchSubmodules = true;
  };

  buildInputs = [ gnumake ];

  installPhase = ''
    mkdir -p $out/bin
    cp ./flk $out/bin
  '';

  meta = {
    platforms = lib.platforms.unix;
    license = lib.licenses.mpl20;
  };
}
