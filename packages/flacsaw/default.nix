{
  writers,
  lib,
  shntool,
  flac,
  cuetools,
  beets,
}:

writers.writePython3Bin "flacsaw" { flakeIgnore = [ "E501" ]; } ''
  import sys
  import time
  import subprocess
  import shutil
  import pathlib
  import tempfile

  args = sys.argv

  if len(args) < 2:
      print("No path provided, exiting")
      sys.exit(1)

  directory = args[1]

  cuesheets = list(pathlib.Path(directory).glob("*.cue"))
  files = list(pathlib.Path(directory).glob("*.flac"))

  if len(cuesheets) != 1:
      print(f"{len(cuesheets)} cuesheets found, exiting")
      sys.exit(1)

  if len(files) != 1:
      print(f"{len(files)} files found, exiting")
      sys.exit(1)

  _cuesheet = cuesheets[0]
  _file = files[0]

  with tempfile.TemporaryDirectory() as temp_dir:
      print(temp_dir)

      cuesheet = shutil.copy(_cuesheet, temp_dir)
      file = shutil.copy(_file, temp_dir)

      subprocess.run(["${shntool}/bin/shnsplit", "-f", cuesheet, "-d", temp_dir, file])

      wav_files = pathlib.Path(temp_dir).glob("*.wav")

      for wav_file in wav_files:
          subprocess.run(["${lib.getExe' flac "flac"}", "--best", wav_file])
          wav_file.unlink()

      artist = subprocess.run(["${cuetools}/bin/cueprint", "-d", "%P", cuesheet], capture_output=True).stdout
      album = subprocess.run(["${cuetools}/bin/cueprint", "-d", "%T", cuesheet], capture_output=True).stdout

      pathlib.Path(file).unlink()
      pathlib.Path(cuesheet).unlink()

      beets = subprocess.Popen(["${beets}/bin/beet", "import", temp_dir], stdin=subprocess.PIPE)

      time.sleep(3)

      # automatically search for release using cuesheet data
      beets.stdin.write(b"E\n" + artist + b"\n" + album + b"\n")
      beets.stdin.flush()

      time.sleep(3)

      # choose which release to use for metadata
      choice = input("")
      beets.stdin.write(choice.encode() + b"\n")
      beets.stdin.flush()

      time.sleep(1)

      # choose whether to apply the changes
      choice = input("")
      beets.communicate(choice.encode() + b"\n")
''
