# Export an attribute set of locally defined packages (from the
# `packages` directory). This is primarily of use with `nix-update`,
# which requires an expression of this format to perform updates.
#
# Usage: nix-update <package> -f ./packages.nix
let
  inputs = (import ./.).inputs;
  defaultPkgs = import inputs.nixpkgs-unstable {
    overlays = [
      (_final: prev: {
        npmlock2nix = (import inputs.npmlock2nix { pkgs = prev; }).v2;
      })
      (import ./overlays/localPackages.nix)
    ];
  };
in
{
  pkgs ? defaultPkgs,
}:
pkgs.localPackages
