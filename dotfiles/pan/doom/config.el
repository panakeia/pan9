;;; .doom.d/config.el -*- lexical-binding: t; -*-

;; Behavior

(setq ;; Make which-key spawn faster
      which-key-idle-delay 0.3

      ;; When splitting windows, always move to the new split
      evil-split-window-below t
      evil-vsplit-window-right t

      ;; Search in all windows with Avy
      avy-all-windows t

      ;; Encrypt new org-roam files with GPG
      org-roam-capture-templates '(("d" "default" plain "%?"
        :target (file+head "${slug}.org.gpg"
                        "#+title: ${title}\n")
        :unnarrowed t)))


;; UI

(setq doom-theme 'modus-vivendi
      doom-font (font-spec :family "Iosevka" :size 16)

      frame-title-format '("%b@emacs")

      ;; Disable line numbers by default
      display-line-numbers-type nil

      ;; Disable tab icons
      centaur-tabs-set-icons nil

      ;; Disable tab close button
      centaur-tabs-set-close-button nil

      ;; Show a smaller modified marker
      centaur-tabs-modified-marker "∗"

      ;; Disable mouseover text for modeline segments
      mode-line-default-help-echo nil
      show-help-function nil
      )

(require 'solaire-mode)
(solaire-global-mode +1)

(pixel-scroll-precision-mode +1)

;; Keybinds

(map! :leader
      ;; Run shell command
      "$"   'shell-command
      ;; Open url at point
      "o u" 'browse-url-at-point
      ;; Copy a password from the password-store
      "o p p" 'password-store-copy
      ;; Copy a password field from the password-store
      "o p f" 'password-store-copy-field
      ;; Copy an otp token from the password-store
      "o p o" 'password-store-otp-token-copy
      ;; Insert a new password in the password-store
      "o p i" 'password-store-insert
      ;; Generate a new password and insert it in the password-store
      "o p g" 'password-store-generate
      ;; Edit an entry in the password-store
      "o p e" 'password-store-edit)

(after! exwm
  (defun exwm-workspace-previous ()
    (interactive)
    (let ((index exwm-workspace-current-index))
      (exwm-workspace-switch (- index 1))))

  (defun exwm-workspace-next ()
    (interactive)
    (let ((index exwm-workspace-current-index))
      (exwm-workspace-switch (+ index 1))))

  (defun lock-screen ()
    (interactive)
    (shell-command "xsecurelock"))

  ;; Define alternate leader keys to be used in EXWM buffers.
  (evil-define-key* '(normal visual motion) general-override-mode-map (kbd "s-SPC") 'doom/leader)

  ;; Force alternate leader keys to be forwarded to Emacs.
  (push ?\s-\  exwm-input-prefix-keys)

  (map!
   ;; Reset (to line-mode).
   "s-r" 'exwm-reset
   ;; Switch workspace.
   "s-w" 'exwm-workspace-switch
   ;; Launch application.
   "s-&" (lambda (command)
           (interactive (list (read-shell-command "$ ")))
           (start-process-shell-command command nil command))
   ;; Switch to workspace N (0 <= N <= 9).
   ;; "s-N" ,@(mapcar (lambda (i)
   ;;                   `(,(kbd (format "s-%d" i)) .
   ;;                     (lambda ()
   ;;                       (interactive)
   ;;                       (exwm-workspace-switch-create ,i))))
   ;;                 (number-sequence 0 9)))))
   ;; Move between buffers.
   "s-h" 'windmove-left
   "s-j" 'windmove-down
   "s-k" 'windmove-up
   "s-l" 'windmove-right
   ;; Move between exwm workspaces.
   "s-[" 'exwm-workspace-previous
   "s-]" 'exwm-workspace-next
   ;; Interact with exwm windows.
   "s-f" 'exwm-floating-toggle-floating
   "s-m" 'exwm-layout-toggle-fullscreen
   ;; Launch graphical application.
   "s-d" 'counsel-linux-app
   ;; Lock screen.
   "s-TAB" 'lock-screen))
