{
  peers = {
    servers = {
      server = {
        publicKey = "+Bkz+8P1DDB6sdWCpaInT9/Jv7djbA7tGXVUa92vgSc=";
        endpoint = "192.168.0.1:51820";
        ip = "10.1.0.1";
        allowedIPs = [ "10.1.0.0/16" ];
        persistentKeepalive = 25;
      };
    };

    clients = {
      client1 = {
        publicKey = "QiprkNlxnluD///IjyaFAHaTM/WVbrea6vT1ZyZ4NQA=";
        ip = "10.1.1.1";
      };
      client2 = {
        publicKey = "96+4PoygXgjauEtPn6vuOllX6VgzRBb+D0AhrKiBcV0=";
        ip = "10.1.1.2";
      };
    };
  };
}
