let
  projectName = "pan9";
in
{
  inherit projectName;

  projectRootFile = "$HOME/src/${projectName}/flake.nix";

  chromeExtensions = {
    uBlockOrigin = "cjpalhdlnbpafiamejdnhcphjbkeiagm";
    vimium = "dbepggeogbaibhgnhhndojpepiihcmeb";
  };

  dnsServers = {
    raw = [
      "2620:fe::fe"
      "9.9.9.9"
      "2620:fe::9"
      "149.112.112.112"
    ];
    withTls = [
      "2620:fe::fe@853#dns.quad9.net"
      "9.9.9.9@853#dns.quad9.net"
      "2620:fe::9@853#dns.quad9.net"
      "149.112.112.112@853#dns.quad9.net"
    ];
    withTlsResolved = [
      "[2620:fe::fe]:853#dns.quad9.net"
      "9.9.9.9:853#dns.quad9.net"
      "[2620:fe::9]:853#dns.quad9.net"
      "149.112.112.112:853#dns.quad9.net"
    ];
  };

  ports = rec {
    ssh.tcp = [ 22 ];
    dns = {
      udp = [ 53 ];
      tcp = [ 853 ];
    };
    http.tcp = [ 80 ];
    smb = {
      udp = [
        137
        138
      ];
      tcp = [
        139
        445
      ];
    };
    https.tcp = [ 443 ];
    rtmp.tcp = [ 1935 ];
    ikev2.udp = [
      500
      4500
    ];
    kdeconnect.tcp = [ 1716 ];
    gitea.tcp = [ 3000 ];
    metasrht.tcp = [ 5000 ];
    gitsrht.tcp = [ 5001 ];
    buildsrht.tcp = [ 5002 ];
    todosrht.tcp = [ 5003 ];
    mansrht.tcp = [ 5004 ];
    dispatchsrht.tcp = [ 5005 ];
    listssrht.tcp = [ 5006 ];
    buildsrht-worker.tcp = [ 5020 ];
    radicale.tcp = [ 5232 ];
    mpd.tcp = [
      6600
      6601
    ];
    owncast.tcp = [ 8080 ];
    tiddlywiki.tcp = [ 8080 ];
    transmission.tcp = [ 9091 ];
    syncthing.udp = [ 21027 ];
    input-leap.tcp = [ 24800 ];
    steam = {
      tcp = [ 27036 ];
      udp = [
        {
          from = 27031;
          to = 27036;
        }
      ];
    };
    wireguard.udp = [ 51820 ];
    iperf = {
      udp = [
        {
          from = 5000;
          to = 10000;
        }
      ];
      tcp = [
        {
          from = 5000;
          to = 10000;
        }
      ];
    };
  };
}
